var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db=require('./src/repo/dbconnection');

var indexRouter = require('./routes/index');
var usersRouter = require('./src/routess/user_router');
var hotelRouter = require('./src/routess/Hotel_router');
var approvalRouter=require('./src/routess/approval_router');
var slotRouter=require('./src/routess/Slot_router');
var authRouter= require('./src/routess/auth_router');
var bookingRouter=require('./src/routess/booking_router');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/hotels', hotelRouter);
app.use('/approvals',approvalRouter);
app.use('/auth',authRouter);
app.use('/slot',slotRouter);
app.use('/booking',bookingRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
