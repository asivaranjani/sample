const mongoose = require('mongoose');
const config=require('../config/config.json');
const hotelschema = new mongoose.Schema({
    Hotelid: {
        type: String,
        required: true

    },
    user: {
        type: String,
        ref: 'User',
        required: true
    },

    Hotelname: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    hotelContactNo: {
        type: String,
        required: true,
     
     
    },
    room:{
        type:[{
        roomtype: {
            type: String,
            required: true
        },
        roomno: {
            type: String,
            range:['a0','a31'],
            required: true
        }
    }]},
    status: {
        type: String,
        default: 'Inactive'
    }
});

module.exports = mongoose.model('Hotel', hotelschema, "hotel");

