
const mongoose = require("mongoose");
const config=require('../config/config.json');
const slotschema = new mongoose.Schema({
    hotelId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Hotel',
        required: true
    },
  date:
    {
        type: Date,
        required: true
    },
    SlotNos: {
        type: [
            {
                roomtype: {
                    type: String,
                    required: true
                },
                roomno: {
                    type: String,
                    required: true
                },
                status: {
                    type: String,
                    default:"available"
                }
                }]
    },
    

});

module.exports = mongoose.model('Slot', slotschema, "slot");
