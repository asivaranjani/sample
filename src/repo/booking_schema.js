
const mongoose = require('mongoose');
const bookingSchema = new mongoose.Schema({

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  hotelId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Hotel',
    required: true
  },
  slotId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Slot',
    required: true
  },

  fromdate: {
    type: String,
    required: true
  },
  todate: {
    type: String,
    required: true
  },

  status: {
    type: String,
    default: 'Available'
  }



});
module.exports = mongoose.model('Booking', bookingSchema, "booking");
