const mongoose = require('mongoose');
const config = require('../config/config.json');

const client = mongoose
.connect(config.db_mongo.db, {
useNewUrlParser: true,
useUnifiedTopology: true,
})
.then(() => {
console.log('mongodb successfully connected\n');
})
.catch((err) => {
console.log(err.message);
});



mongoose.connection.on('connected', () => {
console.log('mongoose connected with mongodb');
});
mongoose.connection.on('error', (err) => {
console.log(err.message, '\n');
});
mongoose.connection.on('disconnected', () => {
console.log('mongoose disconnected from mongodb\n');
});



process.on('SIGINT', async () => {
await mongoose.connection.close();
process.exit(0);
});



module.exports = {
client: client,
conn: mongoose.connection,
};