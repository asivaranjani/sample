const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config/config.json');


const userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    mobile_no: { type: String, required: true },
    role: { type: String, enum: ['Admin', 'customer', 'hotelowner'] }
});

userSchema.methods.generateAuthToken = async function () {
    const token = jwt.sign({ _id: this._id, role: this.role }, config.jwt.jwtPrivateKey);
    return token;
}
module.exports = mongoose.model('User', userSchema, "user");
