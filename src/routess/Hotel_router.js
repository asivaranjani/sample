const express = require('express');
var router = express.Router();

var HotelService = require('../services/HotelService');
var Hotelvalidation = require('../validations/Hotelvalidation');
const hotelowner=require('../middleware/hotelowner');
const auth=require('../middleware/auth');

router.post('/add', [auth,hotelowner],Hotelvalidation.validateHotel(), (req, res) => {
    return HotelService.addHotel(req, res);
});

router.put('/:id', [auth,hotelowner],Hotelvalidation.validateHotel(), (req, res) => {
    return HotelService.updateHotel(req, res);
});


router.delete('/:id',[auth,hotelowner],Hotelvalidation.validateHotel(), (req, res) => {
    return HotelService.deleteHotel(req, res);
})



module.exports = router;
