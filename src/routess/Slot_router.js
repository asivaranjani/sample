const express = require('express');
var router = express.Router();
var SlotService=require('../services/SlotService');
const config=require('../config/config.json');
const admin=require('../middleware/admin');
const auth=require('../middleware/auth');
var slotvalidation = require('../validations/slotvalidation');
const validateObjectById = require('../middleware/validateobjectId')



router.post('/',[auth,admin],slotvalidation.validateSlot(), (req, res) => {
    return SlotService.addSlot(req, res);
}
)

router.get('/GET',slotvalidation.validateSlot(),(req,res) =>{
    return SlotService.getUser(req, res);
})

router.get('/:id',validateObjectById, async (req,res)=>{
    return SlotService.getSlot(req,res);
})
module.exports = router;