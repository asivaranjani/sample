const express = require('express');
var router = express.Router();
var approvalService = require('../services/approvalService');
const auth=require('../middleware/auth');
const admin=require('../middleware/admin');
const config=require('../config/config.json');
router.get('/getdetails',[auth,admin], (req, res) => {
    return approvalService.getDetails(req, res);

});
router.put('/updatedetails',[auth,admin], async (req, res) => {

    return  approvalService.updateDetails(req, res);

});

module.exports = router;