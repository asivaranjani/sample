const express = require('express');
var router = express.Router();
var auth_service = require('../services/auth_service');
var auth_validation=require('../validations/auth_validation');
const config = require('../config/config.json');

router.post('/gettoken',auth_validation.validateAuth(),(req, res) => {
    return auth_service.getToken(req, res);
})
module.exports = router;