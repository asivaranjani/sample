const express = require('express');
var router = express.Router();
var booking_service = require('../services/booking_service');
var booking_validation = require('../validations/booking_validation');
const auth = require('../middleware/auth');
const admin=require('../middleware/admin');
const user = require('../middleware/user');


router.post('/add',[auth,user], booking_validation.validateBooking(), (req, res) => {
    return booking_service.addBooking(req,res);

});
router.get('/:id',[auth,user], booking_validation.validateBooking(), (req, res) => {
    return booking_service.getBooking(req,res);

});


module.exports = router;
