const express = require('express');
var router = express.Router();
var user_service = require('../services/user_service');
var user_validation = require('../validations/user_validation');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const user = require('../middleware/user');


router.post('/add', user_validation.validateUser(), (req, res) => {
    return user_service.addUser(req, res);

});
router.get('/me', [auth, user], user_validation.validateUser(), (req, res) => {
    return user_service.getUser(req, res);
});
router.get('/all', [auth, admin], user_validation.validateUser(), (req, res) => {
    return user_service.getAll(req, res);
});
router.put('/:id', [auth, user], user_validation.validateUser(), (req, res) => {

    return user_service.updateUser(req, res);
});
router.delete('/:id', [auth, user], user_validation.validateUser(), (req, res) => {
    return user_service.deleteUser(req, res);
})



module.exports = router;
