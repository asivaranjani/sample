
const hotel = require('../repo/Hotel_Schema');
const { validationResult, header } = require("express-validator");
//const { count } = require('../repo/Hotel_Schema');
const addHotel = async (req, res) => {
    const errors = validationResult(req);
    console.log("in add hotel method");
    console.log(errors);

    if (errors.isEmpty()) {
        var data = await hotel.findOne({ Hotelname: req.body.Hotelname });
        if (data)

            return res.status(400).send('Hotel already registred')
        data = {
            Hotelid: req.body.Hotelid,
            user: req.body.user,
            Hotelname: req.body.Hotelname,
            location: req.body.location,
            hotelContactNo: req.body.hotelContactNo,
            room: req.body.room
        }
        let result = await hotel.create(data);
        return res.send(result);

    }
    else {
        res.send("validation error")
    }
}

const updateHotel = async (req, res) => {
    const errors = validationResult(req);
    console.log("***updating***");
    console.log(errors);
    if (errors.isEmpty()) {



        let output = await hotel.findByIdAndUpdate(req.params.id, {
            $set: {
                Hotelname: req.body.Hotelname,
                location: req.body.location,
                hotelContactNo: req.body.hotelContactNo,
                room: req.body.room


            }
        }, { new: true })
        return res.send(output);
    }
    else {
        res.send("not updated");
    }
}

const deleteHotel = async (req, res) => {
    const access = await hotel.findByIdAndRemove(req.params.id);
    if (!access)
        return res.status(404).send('The user with given id not found..!');
    res.send(access);
}


module.exports = { addHotel, updateHotel, deleteHotel }



