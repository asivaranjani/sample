const slot = require('../repo/Slot_Schema');

const hotelschema = require('../repo/Hotel_Schema');



const { validationResult, header } = require("express-validator");

const addSlot = async (req, res) => {
    const errors = validationResult(req);
    console.log("in add slot method")
    console.log(errors);
    if (errors.isEmpty()) {
        var teradata = await slot.findOne({ date: req.body.date });
        if (teradata)
            return res.status(400).send('slot already registred')
        var hotel = await hotelschema.findById(req.body.hotelId)
        if (!hotel)
            return res.status(400).send('Invalid hotel');
        teradata = {
            hotelId: req.body.hotelId,
            date: req.body.date,
            SlotNos: req.body.SlotNos
        }
        let output = await slot.create(teradata);
        return res.send(output)
    }


    else {
        res.send("valiadtion error")
    }
}
const getUser = async (req, res) => {
    const slots_data = await slot.find().sort('date')
    res.send(slots_data)
}
const getSlot = async (req, res) => {
    const slots_Data = await slot.findById(req.params.id);
    res.send(slots_Data)
}
module.exports = { addSlot, getUser, getSlot }

