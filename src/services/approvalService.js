
const { validationResult, header } = require("express-validator");
const hotelSchema = require("../repo/Hotel_Schema");

const getDetails = async (req, res) => {
    const errors = validationResult(req);
    console.log("in add hotel method");
    console.log(errors);

    if (errors.isEmpty()) {
        let getdata = await hotelSchema.find({ status: "Inactive" });
        res.send(getdata);

    } else {
        res.send("valiadtion error")
    }
}

const updateDetails = async (req, res) => {

    const errors = validationResult(req);

    console.log("in add hotel method");

    console.log(errors);

    var result = await hotelSchema.updateOne({ status: "Inactive" }, {

        $set: {

            status: "active"
        }
    })

    console.log("result", result);
    return res.send(result)
}

module.exports = { getDetails, updateDetails }