const bcrypt = require('bcrypt');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const userSchema = require("../repo/user_schema");
//const db = require('../repo/db_connection');
const { validationResult, header } = require('express-validator');

const getToken = async (req, res) => {
  const errors = validationResult(req);
  console.log("in auth method")
  console.log(errors)

  if (errors.isEmpty()) {

    let users = await userSchema.findOne({ email: req.body.email });
    if (!users)
      return res.status(400).send('Invalid email or password.');

    let pass =await bcrypt.compare(req.body.password, users.password);
    if (!pass)
      return res.status(400).send('Invalid email or password.');


  const token =await  users.generateAuthToken();
   return res.status(200).send(token);
  }
  else { res.send("validation error") }
}
module.exports = { getToken }



