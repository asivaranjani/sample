
const user = require('../repo/user_schema');
const bookingSchema = require('../repo/booking_schema');
const slot = require('../repo/Slot_Schema');
const hotel = require('../repo/Hotel_Schema');
const { validationResult, header } = require("express-validator");
const express = require('express');

const addBooking = async (req, res) => {
    const errors = validationResult(req);
    console.log("in add booking method")
    console.log(errors)
    if (errors.isEmpty()) {

        const userdata = await user.findById(req.body.userId);
        if (!userdata)
            return res.status(400).send('Invalid user.');


        const hoteldata = await hotel.findById(req.body.hotelId);
        if (!hoteldata)
            return res.status(400).send('Invalid hotel.');

        const slotdata = await slot.findById(req.body.slotId);
        if (!slotdata)
            return res.status(400).send('Invalid slot');

        const check = await bookingSchema.findOne({ fromdate: req.body.fromdate })
        if (check)
            return res.status(400).send('date already registred');


        var bookingdata = {
            userId: req.body.userId,
            hotelId: req.body.hotelId,
            slotId: req.body.slotId,
            fromdate: req.body.fromdate,
            todate: req.body.todate
        };
        let result = await bookingSchema.create(bookingdata);
        let output = await bookingSchema.updateOne({ status: 'Available' }, {
            $set: {
                status: "Booked"
            }
        });

        let change = await slot.updateOne({ slotId: req.body.slotId, "SlotNos.status": 'available' }, {
            $set: {
                "SlotNos.$.status": "Booked"
            }
        });
        console.log(change);
        console.log(output);
        console.log(result);
        return res.send(output);

    }

    else { res.send("validation error") }
};

const getBooking = async (req, res) => {

    const data = await bookingSchema.findById(req.params.id)

    res.send(data)
};
const getSlot = async (req, res) => {

    const slots_data = await slot.find().sort('slotNos')

    res.send(slots_data)
}

module.exports = { addBooking, getBooking, getSlot }