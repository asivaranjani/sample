const userr = require('../repo/user_schema');
const { validationResult, header } = require('express-validator');
const _=require('lodash');
const config=require('../config/config.json');
const bcrypt=require('bcrypt');

const addUser = async (req, res) => {
  const errors = validationResult(req);
  console.log("in add user method")
  console.log(errors)

  if (errors.isEmpty()) {

    let data = await userr.findOne({ email: req.body.email });
    if (data)
      return res.status(400).send('user already registered.');

    data = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      mobile_no: req.body.mobile_no
    };
    data.password=await bcrypt.hash(data.password,10);
    

    let result = await userr.create(data);

    return res.send(result);
  }
  else { res.send("validation error") }

  const token = await userr.generateAuthToken();
  res.header('x-auth-token', token).send(_.pick(userr, ['_id', 'name', 'email', 'mobile_no', 'role']));// create a header , header has a key, value pair and also send the id, name... to the client// pick is a lodash methode// lodash create a new obj and put the details like a id, name, and give it to the router
};

const getAll = async (req, res) => {
  let viewall = await userr.find().select('-password');
  res.send(viewall);

};  

const getUser = async (req, res) => {

  let viewuser = await userr.findById(req.user._id).select('-password');
  res.send(viewuser);
}
const updateUser = async (req, res) => {

  const errors = validationResult(req);

  console.log("in update user method")

  console.log(errors)

  if (errors.isEmpty()) {



    let output = await userr.findByIdAndUpdate(req.params.id, {
      $set: {
        name: req.body.name,

        email: req.body.email,

        password: req.body.password,

        mobile_no: req.body.mobile_no,

        role: req.body.role

      }
    }, { new: true });

    return res.send(output);

  }

  else {

    res.send("validation error");

  }};
  const deleteUser = async (req, res) => {

    const doutput = await userr.findByIdAndRemove(req.params.id);

    if (!doutput)
      return res.status(404).send('The user with given id not found..!');
    res.send(doutput);
  }
module.exports = { addUser, getAll, getUser, updateUser, deleteUser }


