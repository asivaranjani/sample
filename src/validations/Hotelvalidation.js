
const { check, body } = require('express-validator');
const config = require('../config/config.json');
function validateHotel() {
    return [ 
    body("Hotelid", config.fields.notEmpty).notEmpty(),
    body("user", config.fields.notEmpty).notEmpty(),
    body("Hotelname", config.fields.notEmpty).notEmpty(),
    body("location", config.fields.notEmpty).notEmpty(),
    body("hotelContactNo", config.fields.notEmpty).notEmpty(),
    body("room", config.fields.notEmpty).notEmpty()
    ];
}
module.exports = { validateHotel }

