const { check, body } = require('express-validator');

const config = require('../config/config.json');

function validateAuth() {
    return [
       
        body("email", config.fields.notEmpty).notEmpty(),
        body("password", config.fields.notEmpty).notEmpty(),
         ];
}

module.exports = { validateAuth }