const { check, body } = require('express-validator');
const config = require('../config/config.json');

function validateBooking() {
    return [
        body("userId", config.fields.notEmpty).notEmpty(),
        body("hotelId", config.fields.notEmpty).notEmpty(),
        body("slotId", config.fields.notEmpty).notEmpty(),
        body("fromdate", config.fields.notEmpty).notEmpty(),
        body("todate", config.fields.notEmpty).notEmpty()
    ];
    
}

module.exports = { validateBooking }