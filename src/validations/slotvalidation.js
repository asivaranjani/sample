const { check, body } = require('express-validator');
const config = require('../config/config.json');

function validateSlot() {
    return [


        body("hotelId", config.fields.notEmpty).notEmpty(),
        body("date", config.fields.notEmpty).notEmpty(),
        body("SlotNos", config.fields.notEmpty).notEmpty()



    ];
}


module.exports = {
    validateSlot
}