//const Joi = require('joi');
const { check, body } = require('express-validator');

const config = require('../config/config.json');

function validateUser() {
    return [
        body("name", config.fields.notEmpty).notEmpty(),
        body("email", config.fields.notEmpty).notEmpty(),
        body("password", config.fields.notEmpty).notEmpty(),
        body("mobile_no", config.fields.notEmpty).notEmpty(),
        body("role", config.fields.notEmpty).notEmpty()
    ];
}

module.exports = { validateUser }